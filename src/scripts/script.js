

// const loginFormButton = document.querySelector(".loginForm-button");
// const inputEmail = document.querySelector('.loginForm-email');
// const inputPassword = document.querySelector('.loginForm-password')
// let inputEmailValue;
// let inputPasswordValue;

// console.log(loginFormButton)
// loginFormButton.addEventListener("click", function() {
    
//      inputEmailValue = inputEmail.value;
//     console.log(inputEmailValue);
//     inputPasswordValue = inputPassword.value;
//    console.log(inputPasswordValue)

//    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
//   method: 'POST',
//   headers: {
//     'Content-Type': 'application/json'
//   },
//   body: JSON.stringify({ email: inputEmailValue, password: inputPasswordValue })
// })
//   .then(response => response.text())
//   .then(token => console.log(token))
// });

const loginFormButton = document.querySelector(".loginForm-button");
const inputEmail = document.querySelector('.loginForm-email');
const inputPassword = document.querySelector('.loginForm-password');

loginFormButton.addEventListener("click", function() {
    const inputEmailValue = inputEmail.value;
    const inputPasswordValue = inputPassword.value;

    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ email: inputEmailValue, password: inputPasswordValue })
    })
    .then(response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return response.text();
    })
    .then(token => {
        console.log(token);
        // Тут ви можете використовувати токен
    })
    .catch(error => {
        console.error('There was a problem with your fetch operation:', error);
    });
});

